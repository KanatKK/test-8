import axios from "axios";

const axiosQuote = axios.create({
    baseURL: 'https://quotes-3efd3.firebaseio.com/'
});

export default axiosQuote;