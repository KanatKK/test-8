import React, {useEffect, useState} from 'react';
import './quotes.css';
import axiosQuote from "../../axiosQuote";
import Spinner from "../../components/spinner/spinner";

const Quotes = () => {
    const [newQuotes, setNewQuotes] = useState([]);
    const [quote, setQuote] = useState([{response:'/quotes.json'}]);
    const quoteCopy = [...quote];

    useEffect(() => {
        const fetchData = async () => {
            const quotes = await axiosQuote.get(quote[0].response);
            const newQuotesCopy = [...newQuotes];
            newQuotesCopy.length = 0;
            for (const key in quotes.data) {
                newQuotesCopy.push({
                    quote: quotes.data[key].quote,
                    author: quotes.data[key].author,
                    id: key,
                    category: quotes.data[key].category,
                });
            }
            setNewQuotes(newQuotesCopy);
        };
        fetchData().catch(e => console.log(e));
    }, [quote]);

    const quoteList = newQuotes.map(quote => {
        return (
            <div className="quote" key={quote.id}>
                <button type="button" className="delete">X</button>
                <p className="quoteTxt">"{quote.quote}"</p>
                <p className="author">© {quote.author}</p>
                <button type="button" className="edit">Edit</button>
            </div>
        );
    });

    const getCategory = event => {
        if (event.target.innerHTML === 'All') {
            quoteCopy[0] = {response:'/quotes.json'};
            setQuote(quoteCopy);
        } else {
            quoteCopy[0] = {response:'/quotes.json?orderBy="category"&equalTo="'+ event.target.innerHTML +'"'};
            setQuote(quoteCopy);
        }
    };

    if (newQuotes.length !== 0) {
        return (
            <div className="container">
                <div className="quotesBlock">
                    <div className="list">
                        <ul>
                            <li onClick={getCategory} className="link">All</li>
                            <li onClick={getCategory} className="link">Movies</li>
                            <li onClick={getCategory} className="link">Cartoons</li>
                            <li onClick={getCategory} className="link">Famous People</li>
                            <li onClick={getCategory} className="link">Motivational</li>
                            <li onClick={getCategory} className="link">Beautiful</li>
                        </ul>
                    </div>
                    <div className="quotes">
                        <h2 style={{margin: 0}}>All</h2>
                        {quoteList}
                    </div>
                </div>
            </div>
        );
    } else {
        return (<Spinner/>)
    }

};

export default Quotes;