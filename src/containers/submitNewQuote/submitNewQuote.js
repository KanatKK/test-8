import React, {useState} from 'react';
import './submitNewQuote.css'
import axiosQuote from "../../axiosQuote";
import Spinner from "../../components/spinner/spinner";

const SubmitNewQuote = props => {
    const [quotes, setQuotes] = useState({
        category: 'Movies', author: '', quote: '',
    });

    const [loading, setLoading] = useState(false);

    const postDataChanger = event => {
        const name = event.target.name;
        const value = event.target.value;

        setQuotes(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const quoteHandler = async event => {
        event.preventDefault();
        setLoading(true);

        try {
            await axiosQuote.post('/quotes.json', {...quotes});
        } finally {
            setLoading(false);
            props.history.push('/');
        }
    };

    if (loading) {
        return (<Spinner/>)
    } else {
        return (
            <div className="container">
                <form className="submitQuotes" onSubmit={quoteHandler}>
                    <h2>Submit New Quotes</h2>
                    <label htmlFor="category">Choose category: </label>
                    <select name="category" className="categoryChanger" onChange={postDataChanger}>
                        <option value="Movies">Movies</option>
                        <option value="Cartoons">Cartoons</option>
                        <option value="Famous People">Famous People</option>
                        <option value="Motivational">Motivational</option>
                        <option value="Beautiful">Beautiful</option>
                    </select>
                    <input type="text" className="areaForAuthor" name="author" placeholder="Author" onChange={postDataChanger}/>
                    <textarea name="quote" className="areaForQuote" placeholder="Quote" onChange={postDataChanger}/>
                    <button type="submit" className="saveBtn">Save</button>
                </form>
            </div>
        );
    }
};

export default SubmitNewQuote;