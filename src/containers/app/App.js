import React from 'react';
import './App.css';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import Header from "../../components/header/header";
import Quotes from "../quotes/quotes";
import SubmitNewQuote from "../submitNewQuote/submitNewQuote";

const App = () => {
  return (
      <BrowserRouter>
        <Route component={Header}/>
        <Switch>
        <Route path="/" exact component={Quotes}/>
        <Route path="/submitQuote" exact component={SubmitNewQuote}/>
        </Switch>
      </BrowserRouter>
  );
};

export default App;
